//
// Created by Aidan Dunlap on 5/16/24.
//

#include "LevelDB_JNI.h"
#include "leveldb/write_batch.h"
#include "leveldb/db.h"
#include <string>
#include <vector>
#include <map>

std::map<std::string, leveldb::DB*> dbs;

void throwJavaException(JNIEnv* env, const char* message) {
    jclass exceptionClass = env->FindClass("java/lang/Exception");
    if (exceptionClass == nullptr) {
        return;
    }

    env->ThrowNew(exceptionClass, message);
}

std::string base64_decode(const std::string &in) {
    std::string out;
    std::vector<int> T(256, -1);
    for (int i = 0; i < 64; i++) {
        T["ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"[i]] = i;
    }

    int val = 0, valb = -8;
    for (unsigned char c : in) {
        if (T[c] == -1) break;
        val = (val << 6) + T[c];
        valb += 6;
        if (valb >= 0) {
            out.push_back(char((val >> valb) & 0xFF));
            valb -= 8;
        }
    }
    return out;
}

std::string base64_encode(const std::string &in) {
    static const std::string base64_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    std::string out;
    int val = 0, valb = -6;
    for (unsigned char c : in) {
        val = (val << 8) + c;
        valb += 8;
        while (valb >= 0) {
            out.push_back(base64_chars[(val >> valb) & 0x3F]);
            valb -= 6;
        }
    }
    if (valb > -6) out.push_back(base64_chars[((val << 8) >> (valb + 8)) & 0x3F]);
    while (out.size() % 4) out.push_back('=');
    return out;
}






JNIEXPORT void JNICALL Java_org_futo_polycentric_leveldbcapacitor_MobileLevelImpl_db_1init(JNIEnv *, jobject) {
    return;
}

extern "C"
JNIEXPORT void JNICALL
Java_org_futo_polycentric_leveldbcapacitor_MobileLevelImpl_db_1open(JNIEnv *env, jobject ,
                                                                    jstring dbName, jstring path) {
    const char* db_name_cstr = env->GetStringUTFChars(dbName, nullptr);

    leveldb::DB* db = dbs[db_name_cstr];
    if (db) {
        // noop
        env->ReleaseStringUTFChars(dbName, db_name_cstr);
        return;
    }

    const char* path_cstr = env->GetStringUTFChars(path, nullptr);

    leveldb::Options options;
    options.create_if_missing = true;
    leveldb::Status status = leveldb::DB::Open(options, path_cstr, &db);

    if (status.ok()) {
        dbs[db_name_cstr] = db;
        env->ReleaseStringUTFChars(dbName, db_name_cstr);
        env->ReleaseStringUTFChars(path, path_cstr);
    } else {
        // Handle error
        env->ReleaseStringUTFChars(dbName, db_name_cstr);
        env->ReleaseStringUTFChars(path, path_cstr);

        throwJavaException(env, status.ToString().c_str());
    }
    
}

JNIEXPORT jstring JNICALL Java_org_futo_polycentric_leveldbcapacitor_MobileLevelImpl_db_1get(JNIEnv *env, jobject, jstring dbName, jstring key) {
    const char* db_name_cstr = env->GetStringUTFChars(dbName, nullptr);
    leveldb::DB* db = dbs[db_name_cstr];
    if (!db) {
        env->ReleaseStringUTFChars(dbName, db_name_cstr);
        throwJavaException(env, "Database not found");
        
        return nullptr;
    }

    const char* key_cstr = env->GetStringUTFChars(key, nullptr);
    std::string decoded_key = base64_decode(key_cstr);

    std::string value;
    leveldb::Status status = db->Get(leveldb::ReadOptions(), decoded_key, &value);

    env->ReleaseStringUTFChars(key, key_cstr);
    env->ReleaseStringUTFChars(dbName, db_name_cstr);

    if (status.ok() && !status.IsNotFound()) {
        value = base64_encode(value);
        jstring result = env->NewStringUTF(value.c_str());
        return result;
    } else {
        return nullptr;
    }
}

JNIEXPORT void JNICALL Java_org_futo_polycentric_leveldbcapacitor_MobileLevelImpl_db_1put(JNIEnv *env, jobject, jstring dbName, jstring key, jstring value) {
    const char* db_name_cstr = env->GetStringUTFChars(dbName, nullptr);
    leveldb::DB* db = dbs[db_name_cstr];
    if (!db) {
        env->ReleaseStringUTFChars(dbName, db_name_cstr);
        throwJavaException(env, "Database not found");
        
        return;
    }
    
    const char* key_cstr = env->GetStringUTFChars(key, nullptr);
    std::string decoded_key = base64_decode(key_cstr);

    const char* value_cstr = env->GetStringUTFChars(value, nullptr);
    std::string decoded_value = base64_decode(value_cstr);

    leveldb::Status status = db->Put(leveldb::WriteOptions(), decoded_key, decoded_value);

    env->ReleaseStringUTFChars(key, key_cstr);
    env->ReleaseStringUTFChars(value, value_cstr);
    env->ReleaseStringUTFChars(dbName, db_name_cstr);
}

JNIEXPORT void JNICALL Java_org_futo_polycentric_leveldbcapacitor_MobileLevelImpl_db_1delete(JNIEnv *env, jobject, jstring dbName, jstring key) {
    const char* db_name_cstr = env->GetStringUTFChars(dbName, nullptr);
    leveldb::DB* db = dbs[db_name_cstr];
    if (!db) {
        // Handle no database found error
        env->ReleaseStringUTFChars(dbName, db_name_cstr);
        throwJavaException(env, "Database not found");

        return;
    }
    
    const char* key_cstr = env->GetStringUTFChars(key, nullptr);
    std::string decoded_key = base64_decode(key_cstr);

    leveldb::Status status = db->Delete(leveldb::WriteOptions(), decoded_key);

    env->ReleaseStringUTFChars(key, key_cstr);
    env->ReleaseStringUTFChars(dbName, db_name_cstr);
}


JNIEXPORT void JNICALL Java_org_futo_polycentric_leveldbcapacitor_MobileLevelImpl_db_1close(JNIEnv *env, jobject, jstring dbName) {
    const char* db_name_cstr = env->GetStringUTFChars(dbName, nullptr);
    leveldb::DB* db = dbs[db_name_cstr];
    if (!db) {
        env->ReleaseStringUTFChars(dbName, db_name_cstr);
        // No error here
        return;
    }

    delete db;
    dbs.erase(db_name_cstr);
    env->ReleaseStringUTFChars(dbName, db_name_cstr);
}

JNIEXPORT void JNICALL Java_org_futo_polycentric_leveldbcapacitor_MobileLevelImpl_db_1clear(JNIEnv *env, jobject, jstring dbName) {
    const char* db_name_cstr = env->GetStringUTFChars(dbName, nullptr);
    leveldb::DB* db = dbs[db_name_cstr];
    if (!db) {
        env->ReleaseStringUTFChars(dbName, db_name_cstr);
        throwJavaException(env, "Database not found");
        return;
    }

    leveldb::Status status;

    leveldb::Iterator* it = db->NewIterator(leveldb::ReadOptions());
    for (it->SeekToFirst(); it->Valid(); it->Next()) {
        db->Delete(leveldb::WriteOptions(), it->key());
    }
    delete it;
    
    env->ReleaseStringUTFChars(dbName, db_name_cstr);
}

JNIEXPORT void JNICALL Java_org_futo_polycentric_leveldbcapacitor_MobileLevelImpl_db_1batch(JNIEnv *env, jobject, jstring dbName, jobjectArray operations) {
    const char* db_name_cstr = env->GetStringUTFChars(dbName, nullptr);
    leveldb::DB* db = dbs[db_name_cstr];
    if (!db) {
        env->ReleaseStringUTFChars(dbName, db_name_cstr);
        throwJavaException(env, "Database not found");
        return;
    }

    int opCount = env->GetArrayLength(operations);
    leveldb::WriteBatch batch;
    leveldb::Status status;

    for (int i = 0; i < opCount; i++) {
        jobject operationMap = env->GetObjectArrayElement(operations, i);
        jclass mapClass = env->FindClass("java/util/Map");
        jmethodID getMethod = env->GetMethodID(mapClass, "get", "(Ljava/lang/Object;)Ljava/lang/Object;");

        jstring typeKey = env->NewStringUTF("type");
        jstring type = (jstring)env->CallObjectMethod(operationMap, getMethod, typeKey);
        const char* type_cstr = env->GetStringUTFChars(type, nullptr);

        jstring keyKey = env->NewStringUTF("key");
        jstring key = (jstring)env->CallObjectMethod(operationMap, getMethod, keyKey);
        const char* key_cstr = env->GetStringUTFChars(key, nullptr);
        std::string decoded_key = base64_decode(key_cstr);

        if (strcmp(type_cstr, "put") == 0) {
            jstring valueKey = env->NewStringUTF("value");
            jstring value = (jstring)env->CallObjectMethod(operationMap, getMethod, valueKey);
            const char* value_cstr = env->GetStringUTFChars(value, nullptr);
            std::string decoded_value = base64_decode(value_cstr);

            batch.Put(decoded_key, decoded_value);

            env->ReleaseStringUTFChars(value, value_cstr);
        } else if (strcmp(type_cstr, "del") == 0) {
            batch.Delete(decoded_key);
        } else {
            throwJavaException(env, "Invalid operation type");
        }

        env->ReleaseStringUTFChars(key, key_cstr);
        env->ReleaseStringUTFChars(type, type_cstr);
        env->DeleteLocalRef(typeKey);
        env->DeleteLocalRef(keyKey);
    }

    status = db->Write(leveldb::WriteOptions(), &batch);
    if (!status.ok()) {
        printf("Debug: Write operation failed with status: %s\n", status.ToString().c_str());
        throwJavaException(env, status.ToString().c_str());
    } else {
        printf("Debug: Write operation successful\n");
    }

    env->ReleaseStringUTFChars(dbName, db_name_cstr);
}


JNIEXPORT jobject JNICALL Java_org_futo_polycentric_leveldbcapacitor_MobileLevelImpl_db_1iterator(JNIEnv *env, jobject, jstring dbName, jobject options) {
    const char* db_name_cstr = env->GetStringUTFChars(dbName, nullptr);
    leveldb::DB* db = dbs[db_name_cstr];

    if (!db) {
        throwJavaException(env, "No database found");
        return nullptr;
    }

    jclass hashMapClass = env->GetObjectClass(options);
    jmethodID getMethod = env->GetMethodID(hashMapClass, "get", "(Ljava/lang/Object;)Ljava/lang/Object;");
    
    jstring gtKey = env->NewStringUTF("gt");
    jstring gteKey = env->NewStringUTF("gte");
    jstring ltKey = env->NewStringUTF("lt");
    jstring lteKey = env->NewStringUTF("lte");
    jstring limitKey = env->NewStringUTF("limit");
    jstring reverseKey = env->NewStringUTF("reverse");

    jstring gt = (jstring)env->CallObjectMethod(options, getMethod, gtKey);
    jstring gte = (jstring)env->CallObjectMethod(options, getMethod, gteKey);
    jstring lt = (jstring)env->CallObjectMethod(options, getMethod, ltKey);
    jstring lte = (jstring)env->CallObjectMethod(options, getMethod, lteKey);
    jobject limitObject = env->CallObjectMethod(options, getMethod, limitKey);
    
    jint limit = 0;
    if (limitObject != nullptr) {
        jclass integerClass = env->FindClass("java/lang/Integer");
        jmethodID intValueMethod = env->GetMethodID(integerClass, "intValue", "()I");
        limit = env->CallIntMethod(limitObject, intValueMethod);
        env->DeleteLocalRef(integerClass);
    }
    jobject reverseObject = env->CallObjectMethod(options, getMethod, reverseKey);
    jboolean reverse = JNI_FALSE;
    if (reverseObject != nullptr) {
        jclass booleanClass = env->FindClass("java/lang/Boolean");
        jmethodID booleanValueMethod = env->GetMethodID(booleanClass, "booleanValue", "()Z");
        reverse = env->CallBooleanMethod(reverseObject, booleanValueMethod);
        env->DeleteLocalRef(booleanClass);
    }
    leveldb::Iterator* iterator = db->NewIterator(leveldb::ReadOptions());
    jclass arrayListClass = env->FindClass("java/util/ArrayList");
    jmethodID arrayListInit = env->GetMethodID(arrayListClass, "<init>", "()V");
    jmethodID arrayListAdd = env->GetMethodID(arrayListClass, "add", "(Ljava/lang/Object;)Z");
    jobject resultList = env->NewObject(arrayListClass, arrayListInit);

    if (gt) {
        const char* gt_cstr = env->GetStringUTFChars(gt, nullptr);
        std::string startKey = base64_decode(gt_cstr);
        env->ReleaseStringUTFChars(gt, gt_cstr);  // Release memory
        iterator->Seek(leveldb::Slice(startKey));
        if (iterator->Valid() && iterator->key().ToString() == startKey) {
            iterator->Next();
        }
    } else if (gte) {
        const char* gte_cstr = env->GetStringUTFChars(gte, nullptr);
        std::string startKey = base64_decode(gte_cstr);
        env->ReleaseStringUTFChars(gte, gte_cstr);  // Release memory
        iterator->Seek(leveldb::Slice(startKey));
    } else {
        iterator->SeekToFirst();
    }

    std::string endKey;
    if (lt) {
        const char* lt_cstr = env->GetStringUTFChars(lt, nullptr);
        endKey = base64_decode(lt_cstr);
        env->ReleaseStringUTFChars(lt, lt_cstr);  // Release memory
    } else if (lte) {
        const char* lte_cstr = env->GetStringUTFChars(lte, nullptr);
        endKey = base64_decode(lte_cstr);
        env->ReleaseStringUTFChars(lte, lte_cstr);  // Release memory
    }

    int count = 0;
    while (iterator->Valid() && ((limit == -1) || count < limit)) {
        std::string key = iterator->key().ToString();

        bool isLessThan = lt && key < endKey;
        bool isLessThanOrEqual = lte && key <= endKey;
        bool shouldInclude = (!lt && !lte) || isLessThan || isLessThanOrEqual;

        if (shouldInclude) {
            std::string value = iterator->value().ToString();

            std::string encodedKey = base64_encode(key);
            std::string encodedValue = base64_encode(value);

            jstring jKey = env->NewStringUTF(encodedKey.c_str());
            jstring jValue = env->NewStringUTF(encodedValue.c_str());

            jobject entry = env->NewObject(arrayListClass, arrayListInit);
            env->CallBooleanMethod(entry, arrayListAdd, jKey);
            env->CallBooleanMethod(entry, arrayListAdd, jValue);

            env->CallBooleanMethod(resultList, arrayListAdd, entry);

            iterator->Next();
            count++;
        } else {
            break;
        }
    }

    if (reverse) {
        jclass collectionsClass = env->FindClass("java/util/Collections");
        jmethodID reverseMethod = env->GetStaticMethodID(collectionsClass, "reverse", "(Ljava/util/List;)V");
        env->CallStaticVoidMethod(collectionsClass, reverseMethod, resultList);
    }

    // Cleanup
    delete iterator;
    env->ReleaseStringUTFChars(dbName, db_name_cstr);
    return resultList;

}