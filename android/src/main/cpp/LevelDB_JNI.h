//
// Created by Aidan Dunlap on 5/16/24.
//

#ifndef ANDROID_LEVELDB_JNI_H
#define ANDROID_LEVELDB_JNI_H

#include <jni.h>
#include <string>

extern "C" {
//JNIEXPORT void JNICALL Java_org_futo_polycentric_leveldbcapacitor_MobileLevelImpl_db_1init(JNIEnv *, jobject);
JNIEXPORT void JNICALL Java_org_futo_polycentric_leveldbcapacitor_MobileLevelImpl_db_1open(JNIEnv *, jobject, jstring, jstring);
JNIEXPORT jstring JNICALL Java_org_futo_polycentric_leveldbcapacitor_MobileLevelImpl_db_1get(JNIEnv *, jobject, jstring, jstring);
JNIEXPORT void JNICALL Java_org_futo_polycentric_leveldbcapacitor_MobileLevelImpl_db_1put(JNIEnv *, jobject, jstring, jstring, jstring);
JNIEXPORT void JNICALL Java_org_futo_polycentric_leveldbcapacitor_MobileLevelImpl_db_1delete(JNIEnv *, jobject, jstring, jstring);
JNIEXPORT void JNICALL Java_org_futo_polycentric_leveldbcapacitor_MobileLevelImpl_db_1close(JNIEnv *, jobject, jstring);
JNIEXPORT void JNICALL Java_org_futo_polycentric_leveldbcapacitor_MobileLevelImpl_db_1clear(JNIEnv *, jobject, jstring);
JNIEXPORT void JNICALL Java_org_futo_polycentric_leveldbcapacitor_MobileLevelImpl_db_1batch(JNIEnv *, jobject, jstring, jobjectArray);
JNIEXPORT jobject JNICALL Java_org_futo_polycentric_leveldbcapacitor_MobileLevelImpl_db_1iterator(JNIEnv *, jobject, jstring, jobject);

}

#endif //ANDROID_LEVELDB_JNI_H
