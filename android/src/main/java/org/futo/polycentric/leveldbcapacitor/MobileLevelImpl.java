package org.futo.polycentric.leveldbcapacitor;

import com.getcapacitor.JSArray;
import com.getcapacitor.JSObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MobileLevelImpl {
    // Add your native methods here
    // Example native methods declarations
    public native void db_open(String dbName, String path);
    public native String db_get(String dbName, String key) throws Exception;
    public native void db_put(String dbName, String key, String value) throws Exception;
    public native void db_delete(String dbName, String key);
    public native void db_clear(String dbName) throws Exception;
    public native void db_close(String dbName);
    public native void db_batch(String dbName, HashMap<String, String>[] operations);
    public native ArrayList<ArrayList<String>> db_iterator(String dbName, HashMap<String, Object> options);

    static {
        System.loadLibrary("leveldb_jni");
    }
}