package org.futo.polycentric.leveldbcapacitor;

import com.getcapacitor.JSObject;

public class MobileLevelCallbacks {
    private final SuccessCallback successCallback;
    private final FailureCallback failureCallback;

    public interface SuccessCallback {
        void onSuccess(JSObject result);
    }

    public interface FailureCallback {
        void onFailure(String error);
    }

    public MobileLevelCallbacks(SuccessCallback success, FailureCallback failure) {
        this.successCallback = success;
        this.failureCallback = failure;
    }

    public void onSuccessWithResult(JSObject result) {
        successCallback.onSuccess(result);
    }

    public void onFailure(String error) {
        failureCallback.onFailure(error);
    }
}