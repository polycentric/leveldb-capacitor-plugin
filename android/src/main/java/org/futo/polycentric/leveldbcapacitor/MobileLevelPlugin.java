package org.futo.polycentric.leveldbcapacitor;

import com.getcapacitor.JSArray;
import com.getcapacitor.JSObject;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;
import com.getcapacitor.annotation.CapacitorPlugin;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

@CapacitorPlugin(name = "MobileLevel")
public class MobileLevelPlugin extends Plugin {

    private MobileLevelImpl mobileLevel = new MobileLevelImpl();

    @PluginMethod
    public void open(PluginCall call) {
        String dbName = call.getString("dbName");
        if (dbName == null) {
            call.reject("Missing dbName");
            return;
        }

        File levelDBDir = new File(getContext().getFilesDir(), "leveldb");
        if (!levelDBDir.exists()) {
            levelDBDir.mkdirs();
        }

        // combine dir and db name
        String dbPath = new File(levelDBDir, dbName).getAbsolutePath();

        try {
            mobileLevel.db_open(dbName, dbPath);
            call.resolve();
        } catch (Exception e) {
            call.reject(e.getMessage());
        }
    }

    @PluginMethod
    public void get(PluginCall call) {
        String dbName = call.getString("dbName");
        String key = call.getString("key");
        if (dbName == null || key == null) {
            call.reject("Missing dbName or key");
            return;
        }

        try {
            var result = new JSObject();
            result.put("value", mobileLevel.db_get(dbName, key));
            call.resolve(result);
        } catch (Exception e) {
            call.reject(e.getMessage());
        }
    }

    @PluginMethod
    public void put(PluginCall call) {
        String dbName = call.getString("dbName");
        String key = call.getString("key");
        String value = call.getString("value");
        if (dbName == null || key == null || value == null) {
            call.reject("Missing dbName, key, or value");
            return;
        }

        try {
            mobileLevel.db_put(dbName, key, value);
            call.resolve();
        } catch (Exception e) {
            call.reject(e.getMessage());
        }
    }

    @PluginMethod
    public void delete(PluginCall call) {
        String dbName = call.getString("dbName");
        String key = call.getString("key");
        if (dbName == null || key == null) {
            call.reject("Missing dbName or key");
            return;
        }

        try {
            mobileLevel.db_delete(dbName, key);
            call.resolve();
        } catch (Exception e) {
            call.reject(e.getMessage());
        }
    }

    @PluginMethod
    public void clear(PluginCall call) {
        String dbName = call.getString("dbName");
        if (dbName == null) {
            call.reject("Missing dbName");
            return;
        }

        try {
            mobileLevel.db_clear(dbName);
            call.resolve();
        } catch (Exception e) {
            call.reject(e.getMessage());
        }
    }

    @PluginMethod
    public void close(PluginCall call) {
        String dbName = call.getString("dbName");
        if (dbName == null) {
            call.reject("Missing dbName");
            return;
        }

        try {
            mobileLevel.db_close(dbName);
            call.resolve();
        } catch (Exception e) {
            call.reject(e.getMessage());
        }
    }

    @PluginMethod
    public void batch(PluginCall call) throws JSONException {
        String dbName = call.getString("dbName");
        JSArray operations = call.getArray("operations");
        if (dbName == null || operations == null) {
            call.reject("Missing dbName or operations");
            return;
        }

        try {
            HashMap<String, String>[] javaOperations = new HashMap[operations.length()];
            for (int i = 0; i < operations.length(); i++) {
                JSONObject operation = operations.getJSONObject(i); // corrected to getJSONObject(i)
                HashMap<String, String> javaOperation = new HashMap<>();
                javaOperation.put("type", operation.getString("type"));
                javaOperation.put("key", operation.getString("key"));
                if (operation.has("value")) {
                    javaOperation.put("value", operation.getString("value"));
                }
                javaOperations[i] = javaOperation;
            }

            mobileLevel.db_batch(dbName, javaOperations);
            call.resolve();
        } catch (Exception e) {
            call.reject(e.getMessage());
        }
    }

    @PluginMethod
    public void iterator(PluginCall call) {
        String dbName = call.getString("dbName");
        JSObject options = call.getObject("options");
        if (dbName == null || options == null) {
            call.reject("Missing dbName or options");
            return;
        }

        try {
            HashMap<String, Object> java_options = new HashMap<>();
            for (Iterator<String> it = options.keys(); it.hasNext();) {
                String key = it.next();
                Object value = options.get(key);

                java_options.put(key, value);
            }

            var output = new JSObject();

            // convert the iterator results from an Arraylist<ArrayList<String>> to JSArrays
            var results = new JSArray();
            List<ArrayList<String>> iteratorResults = mobileLevel.db_iterator(dbName, java_options);
            for (List<String> result : iteratorResults) {
                var jsResult = new JSArray();
                for (String value : result) {
                    jsResult.put(value);
                }
                results.put(jsResult);
            }

            output.put("results", results);
            call.resolve(output);
        } catch (Exception e) {
            call.reject(e.getMessage());
        }
    }
}
