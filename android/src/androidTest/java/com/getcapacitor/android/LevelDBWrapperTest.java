package com.getcapacitor.android;

import static org.junit.Assert.*;

import android.content.Context;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;
import java.io.File;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.futo.polycentric.leveldbcapacitor.MobileLevelImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class LevelDBWrapperTest {

    private MobileLevelImpl dbWrapper;

    @Before
    public void setUp() {
        dbWrapper = new MobileLevelImpl();
        var context = ApplicationProvider.getApplicationContext();
        File levelDBDir = new File(context.getFilesDir(), "leveldb");
        if (!levelDBDir.exists()) {
            levelDBDir.mkdirs();
        }

        // combine dir and db name
        String dbPath = new File(levelDBDir, "testdb").getAbsolutePath();

        dbWrapper.db_open("testdb", dbPath);
    }

    @After
    public void tearDown() throws Exception {
        dbWrapper.db_clear("testdb");
        dbWrapper.db_close("testdb");
    }

    @Test
    public void testPutAndGet() throws Exception {
        String key = "foo";
        String value = "bar";

        String key_b64 = Base64.getEncoder().encodeToString(key.getBytes());
        String value_b64 = Base64.getEncoder().encodeToString(value.getBytes());

        dbWrapper.db_put("testdb", key_b64, value_b64);
        String result = dbWrapper.db_get("testdb", key_b64);
        assertEquals(value_b64, result);
    }

    @Test
    public void testDelete() throws Exception {
        dbWrapper.db_put("testdb", "key1", "value1");
        dbWrapper.db_delete("testdb", "key1");
        String value = dbWrapper.db_get("testdb", "key1");
        assertNull(value);
    }

    @Test
    public void testClear() throws Exception {
        dbWrapper.db_put("testdb", "key1", "value1");
        dbWrapper.db_put("testdb", "key2", "value2");
        dbWrapper.db_clear("testdb");
        assertNull(dbWrapper.db_get("testdb", "key1"));
        assertNull(dbWrapper.db_get("testdb", "key2"));
    }

    @Test
    public void testInputThatsBase64WithNullChars() throws Exception {
        dbWrapper.db_put("testdb", "key1", "SGVsbG8AIFdvcmxk");
        String value = dbWrapper.db_get("testdb", "key1");
        assertEquals("SGVsbG8AIFdvcmxk", value);
    }

    @Test
    public void testBatch() throws Exception {
        // Testing batch operations with base64 encoded keys and values
        String batchKey1 = "batchKey1";
        String batchValue1 = "batchValue1";
        String batchKey2 = "batchKey2";
        String batchValue2 = "batchValue2";

        String batchKey1_b64 = Base64.getEncoder().encodeToString(batchKey1.getBytes());
        String batchValue1_b64 = Base64.getEncoder().encodeToString(batchValue1.getBytes());
        String batchKey2_b64 = Base64.getEncoder().encodeToString(batchKey2.getBytes());
        String batchValue2_b64 = Base64.getEncoder().encodeToString(batchValue2.getBytes());

        // Perform batch put operations
        HashMap<String, String>[] operations = new HashMap[2];
        HashMap<String, String> operation1 = new HashMap<>();
        operation1.put("type", "put");
        operation1.put("key", batchKey1_b64);
        operation1.put("value", batchValue1_b64);
        operations[0] = operation1;

        HashMap<String, String> operation2 = new HashMap<>();
        operation2.put("type", "put");
        operation2.put("key", batchKey2_b64);
        operation2.put("value", batchValue2_b64);
        operations[1] = operation2;

        dbWrapper.db_batch("testdb", operations);

        // Verify the batch put operations
        String result1 = dbWrapper.db_get("testdb", batchKey1_b64);
        assertEquals(batchValue1_b64, result1);

        String result2 = dbWrapper.db_get("testdb", batchKey2_b64);
        assertEquals(batchValue2_b64, result2);

    }
}


