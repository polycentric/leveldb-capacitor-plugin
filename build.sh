esbuild src/index.ts \
  --bundle \
  --outfile=dist/plugin.js \
  --format=iife \
  --global-name=capacitorMobileLevel \
  --external:@capacitor/core

#   build cjs
esbuild src/index.ts \
  --bundle \
  --outfile=dist/plugin.cjs.js \
  --format=cjs \
  --external:@capacitor/core

esbuild src/index.ts \
  --bundle \
  --outfile=dist/esm/plugin.js \
  --format=esm \
  --external:@capacitor/core

  cp dist/esm/plugin.js dist/esm/index.js