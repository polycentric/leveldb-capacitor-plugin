#import <leveldb/db.h>
#import <leveldb/options.h>
#import <leveldb/write_batch.h>

#import "MobileLevelImpl.h"

NSDictionary * EMPTY_SUCCESS = nil;

@implementation MobileLevelCallbacks

- (instancetype)initWithSuccess:(void (^)(NSDictionary *))success
                        failure:(void (^)(NSString *))failure {
    self = [super init];
    if (self) {
        _successCallback = [success copy];
        _failureCallback = [failure copy];
    }
    return self;
}

- (void)onSuccessWithResult:(NSDictionary *)result {
    self.successCallback(result);
}

- (void)onFailure:(NSString *)error {
    self.failureCallback(error);
}

@end

@implementation MobileLevelImpl
// internal
// The reason we decode to std::string is because leveldb::Slice, the input to most functions,
// have a constructor that does zero-copying for keys (creates leveldb::Slice's for them) when using std::string
// And I'm worried if we allocate slices ourselves, the runtime might free the underlying memory at the exit of the function
// We could also use a char array, but then we'd have to keep track of lengths ourselves
- (std::string) _decodeBase64:(NSString *)base64 {
    if (!base64) {
        return std::string();
    }
    NSData *data = [[NSData alloc] initWithBase64EncodedString:base64 options:0];
    return std::string(reinterpret_cast<const char *>(data.bytes), data.length);
}

- (NSString *) _encodeBase64:(std::string)str {
    NSData *data = [NSData dataWithBytes:str.c_str() length:str.length()];
    return [data base64EncodedStringWithOptions:0];
}

- (void)_returnLevelDBError:(leveldb::Status)status errorHandler:(MobileLevelCallbacks*)params {
    std::string error_message = status.ToString();
    NSString *error_message_ns = [NSString stringWithUTF8String:error_message.c_str()];
    [params onFailure:error_message_ns];
}

- (void)_returnNoDBFoundError:(MobileLevelCallbacks*)params {
    NSString *error_message_ns = @"No database found";
    [params onFailure:error_message_ns];
}

// public
- (id)init {
//    self = [super init];
    if (self) {
        self.dbs = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (void)db_open:(NSString *)dbName callbacks:(MobileLevelCallbacks *)callbacks{
    leveldb::Options options;
    options.create_if_missing = true;
    
    NSValue *dbValue = [self.dbs objectForKey:dbName];
    leveldb::DB* db = (leveldb::DB *)[dbValue pointerValue];
    if (db) {
        [callbacks onSuccessWithResult:EMPTY_SUCCESS];
        return;
    }

    // get document directory
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];

    // append dbname to document directory
    NSString * path = [documentDirectory stringByAppendingPathComponent:dbName];
    
    std::string cpp_path = [path UTF8String];
    
    leveldb::Status status = leveldb::DB::Open(options, cpp_path, &db);
    if (status.ok()) {
        [self.dbs setObject:[NSValue valueWithPointer:db] forKey:dbName];
        // call  onSuccessWithResult params on success
        [callbacks onSuccessWithResult:EMPTY_SUCCESS];
    } else {
        [self _returnLevelDBError:status errorHandler:callbacks];
    }
}

- (void)db_get:(NSString *)dbName key:(NSString *)key callbacks:(MobileLevelCallbacks *)callbacks {
    NSValue *dbValue = [self.dbs objectForKey:dbName];
    leveldb::DB* db = (leveldb::DB *)[dbValue pointerValue];

    if (!db) {
        [self _returnNoDBFoundError:callbacks];
        return;
    }

    // decode key from base64
    std::string decoded_key = [self _decodeBase64:key];
    std::string decoded_value;

    leveldb::Status status = db->Get(leveldb::ReadOptions(), decoded_key, &decoded_value);
    if (status.ok()) {
        NSDictionary *data = @{@"value": [self _encodeBase64:decoded_value]};
        [callbacks onSuccessWithResult:data];
    } else {
        // if key not found, return nil
        if (status.IsNotFound()) {
            [callbacks onSuccessWithResult:EMPTY_SUCCESS];
        } else {
            [self _returnLevelDBError:status errorHandler:callbacks];
        }
    }
}

- (void)db_put:(NSString *)dbName key:(NSString *)key value:(NSString *)value callbacks:(MobileLevelCallbacks *)callbacks {
    NSValue *dbValue = [self.dbs objectForKey:dbName];
    leveldb::DB* db = (leveldb::DB *)[dbValue pointerValue];

    if (!db) {
        [self _returnNoDBFoundError:callbacks];
        return;
    }
    
    std::string decoded_key = [self _decodeBase64:key];
    std::string decoded_value = [self _decodeBase64:value];
    
    leveldb::Status status = db->Put(leveldb::WriteOptions(), decoded_key, decoded_value);
    if (status.ok()) {
        [callbacks onSuccessWithResult:EMPTY_SUCCESS];
    } else {
        [self _returnLevelDBError:status errorHandler:callbacks];
    }
}

- (void)db_delete:(NSString *)dbName key:(NSString *)key callbacks:(MobileLevelCallbacks *)callbacks {
    NSValue *dbValue = [self.dbs objectForKey:dbName];
    leveldb::DB* db = (leveldb::DB *)[dbValue pointerValue];

    if (!db) {
        [self _returnNoDBFoundError:callbacks];
        return;
    }
    
    std::string decoded_key = [self _decodeBase64:key];
    
    leveldb::Status status = db->Delete(leveldb::WriteOptions(), decoded_key);
    if (status.ok()) {
        [callbacks onSuccessWithResult:EMPTY_SUCCESS];
    } else {
        [self _returnLevelDBError:status errorHandler:callbacks];
    }
}

- (void)db_close:(NSString *)dbName callbacks:(MobileLevelCallbacks *)callbacks {
    NSValue *dbValue = [self.dbs objectForKey:dbName];
    leveldb::DB* db = (leveldb::DB *)[dbValue pointerValue];

    if (!db) {
        [self _returnNoDBFoundError:callbacks];
        return;
    }
    
    delete db;
    [callbacks onSuccessWithResult:EMPTY_SUCCESS];
}

- (void)db_batch:(NSString *)dbName operations:(NSArray<NSDictionary *> *)operations callbacks:(MobileLevelCallbacks *)callbacks {
    leveldb::WriteBatch batch;

    NSValue *dbValue = [self.dbs objectForKey:dbName];
    leveldb::DB* db = (leveldb::DB *)[dbValue pointerValue];

    if (!db) {
        [self _returnNoDBFoundError:callbacks];
        return;
    }
    
    for (NSDictionary *operation in operations) {
        NSString *type = [operation objectForKey:@"type"];
        NSString *key = [operation objectForKey:@"key"];
        NSString *value = [operation objectForKey:@"value"];
        
        std::string decoded_key = [self _decodeBase64:key];
        std::string decoded_value = [self _decodeBase64:value];
        
        if ([type isEqualToString:@"put"]) {
            batch.Put(decoded_key, decoded_value);
        } else if ([type isEqualToString:@"del"]) {
            batch.Delete(decoded_key);
        }
    }
    
    leveldb::Status status = db->Write(leveldb::WriteOptions(), &batch);
    if (status.ok()) {
        [callbacks onSuccessWithResult:EMPTY_SUCCESS];
    } else {
        [self _returnLevelDBError:status errorHandler:callbacks];
    }
}

- (void)db_iterator:(NSString *)dbName options:(NSDictionary *)options callbacks:(MobileLevelCallbacks *)callbacks {
    NSValue *dbValue = [self.dbs objectForKey:dbName];
    leveldb::DB* db = (leveldb::DB *)[dbValue pointerValue];

    if (!db) {
        [self _returnNoDBFoundError:callbacks];
        return;
    }

    NSString *gt = [options objectForKey:@"gt"];
    NSString *gte = [options objectForKey:@"gte"];
    NSString *lt = [options objectForKey:@"lt"];
    NSString *lte = [options objectForKey:@"lte"];
    NSNumber *limit = [options objectForKey:@"limit"];
    NSNumber *reverse = [options objectForKey:@"reverse"];

    leveldb::Iterator* it = db->NewIterator(leveldb::ReadOptions());
    NSMutableArray *results = [NSMutableArray array];

    // Determine start point
    std::string startKey;
    if (gt) {
        startKey = [self _decodeBase64:gt];
        it->Seek(leveldb::Slice(startKey));
        if (it->Valid() && it->key().ToString() == startKey) {
            it->Next();
        }
    } else if (gte) {
        startKey = [self _decodeBase64:gte];
        it->Seek(leveldb::Slice(startKey));
    } else {
        it->SeekToFirst();
    }
    
    // Determine end condition
    std::string endKey;
    if (lt) {
        endKey = [self _decodeBase64:lt];
    } else if (lte) {
        endKey = [self _decodeBase64:lte];
    }
    
    // Collect results
    int count = 0;
    int limitInt = [limit intValue];
    while (it->Valid() && ((limitInt == -1) || (count < limitInt))) {
        std::string currentKey = it->key().ToString();
        bool isLessThan = lt && currentKey < endKey;
        bool isLessThanOrEqual = lte && currentKey <= endKey;
        bool shouldInclude = (!lt && !lte) || isLessThan || isLessThanOrEqual;

        if (shouldInclude) {
            NSString *key = [self _encodeBase64:currentKey];
            NSString *value = [self _encodeBase64:it->value().ToString()];
            NSArray *result = @[key, value];
            [results addObject:result];
            count++;
        } else {
            break;
        }
        it->Next();
    }
    
    delete it;

    if ([reverse boolValue]) {
        NSArray *reversedResults = [[results reverseObjectEnumerator] allObjects];
        NSDictionary *resultsDict = @{@"results": reversedResults};
        [callbacks onSuccessWithResult:resultsDict];
    } else {
        NSDictionary *resultsDict = @{@"results": results};
        [callbacks onSuccessWithResult:resultsDict];
    }
}

- (void)db_clear:(NSString *)dbName callbacks:(MobileLevelCallbacks *)callbacks {
    NSValue *dbValue = [self.dbs objectForKey:dbName];
    leveldb::DB* db = (leveldb::DB *)[dbValue pointerValue];

    if (!db) {
        [self _returnNoDBFoundError:callbacks];
        return;
    }

    leveldb::WriteBatch batch;
    leveldb::Iterator* it = db->NewIterator(leveldb::ReadOptions());
    for (it->SeekToFirst(); it->Valid(); it->Next()) {
        batch.Delete(it->key());
    }
    delete it;
    
    leveldb::Status status = db->Write(leveldb::WriteOptions(), &batch);
    if (status.ok()) {
        [callbacks onSuccessWithResult:EMPTY_SUCCESS];
    } else {
        [self _returnLevelDBError:status errorHandler:callbacks];
    }
}


@end
