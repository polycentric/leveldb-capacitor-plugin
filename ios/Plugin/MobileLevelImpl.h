#ifndef MobileLevelImpl_h
#define MobileLevelImpl_h

#import <Foundation/Foundation.h>


@interface MobileLevelCallbacks : NSObject

@property (nonatomic, copy) void (^successCallback)(NSDictionary *);
@property (nonatomic, copy) void (^failureCallback)(NSString *);

- (instancetype)initWithSuccess:(void (^)(NSDictionary *))success
                        failure:(void (^)(NSString *))failure;

- (void)onSuccessWithResult:(NSDictionary *)result;
- (void)onFailure:(NSString *)error;

@end

@interface MobileLevelImpl : NSObject

@property (nonatomic, strong) NSMutableDictionary *dbs;

- (instancetype)init;

// take in string and callbacks object
- (void)db_open:(NSString *)dbName callbacks:(MobileLevelCallbacks *)callbacks;
- (void)db_get:(NSString *)dbName key:(NSString *)key callbacks:(MobileLevelCallbacks *)callbacks;
- (void)db_put:(NSString *)dbName key:(NSString *)key value:(NSString *)value callbacks:(MobileLevelCallbacks *)callbacks;
- (void)db_delete:(NSString *)dbName key:(NSString *)key callbacks:(MobileLevelCallbacks *)callbacks;
- (void)db_close:(NSString *)dbName callbacks:(MobileLevelCallbacks *)callbacks;
- (void)db_batch:(NSString *)dbName operations:(NSArray<NSDictionary *> *)operations callbacks:(MobileLevelCallbacks *)callbacks;
- (void)db_iterator:(NSString *)dbName options:(NSDictionary *)options callbacks:(MobileLevelCallbacks *)callbacks;
- (void)db_clear:(NSString *)dbName callbacks:(MobileLevelCallbacks *)callbacks;


@end
#endif /* MobileLevelImpl_h */
