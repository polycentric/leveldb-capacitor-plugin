import Foundation
import Capacitor

@objc(MobileLevel)
public class MobileLevel: CAPPlugin {

    @objc var mobileLevel: MobileLevelImpl = MobileLevelImpl()

    @objc public func open(_ call: CAPPluginCall) {
        let name = call.getString("dbName")

        let callbacks = MobileLevelCallbacks(success: { (result) in
            if let r = result as? [String: Any] {
                call.resolve(r)
            } else {
                call.resolve()
            }
            return
        }, failure: { (error) in
            call.reject(error ?? "Unknown error")
            print(error)
            return
        })
        
        mobileLevel.db_open(name, callbacks: callbacks)
    }

    @objc public func get(_ call: CAPPluginCall) {
        guard let dbName = call.getString("dbName"), let key = call.getString("key") else {
            call.reject("Missing dbName or key")
            return
        }

        let callbacks = MobileLevelCallbacks(success: { (result) in
            if let r = result as? [String: Any] {
                call.resolve(r)
            } else {
                call.resolve()
            }
        }, failure: { (error) in
            call.reject(error ?? "Unknown error")
        })

        mobileLevel.db_get(dbName, key: key, callbacks: callbacks)
    }

    @objc public func put(_ call: CAPPluginCall) {
        guard let dbName = call.getString("dbName"), let key = call.getString("key"), let value = call.getString("value") else {
            call.reject("Missing dbName, key, or value")
            return
        }

        let callbacks = MobileLevelCallbacks(success: { (result) in
            if let r = result as? [String: Any] {
                call.resolve(r)
            } else {
                call.resolve()
            }
        }, failure: { (error) in
            call.reject(error ?? "Unknown error")
        })

        mobileLevel.db_put(dbName, key: key, value: value, callbacks: callbacks)
    }

    @objc public func delete(_ call: CAPPluginCall) {
        guard let dbName = call.getString("dbName"), let key = call.getString("key") else {
            call.reject("Missing dbName or key")
            return
        }

        let callbacks = MobileLevelCallbacks(success: { _ in
            call.resolve()
        }, failure: { (error) in
            call.reject(error ?? "Unknown error")
        })

        mobileLevel.db_delete(dbName, key: key, callbacks: callbacks)
    }

    @objc public func clear(_ call: CAPPluginCall) {
        guard let dbName = call.getString("dbName") else {
            call.reject("Missing dbName")
            return
        }

        let callbacks = MobileLevelCallbacks(success: { _ in
            call.resolve()
        }, failure: { (error) in
            call.reject(error ?? "Unknown error")
        })

        mobileLevel.db_clear(dbName, callbacks: callbacks)
    }

    @objc public func close(_ call: CAPPluginCall) {
        guard let dbName = call.getString("dbName") else {
            call.reject("Missing dbName")
            return
        }

        let callbacks = MobileLevelCallbacks(success: { _ in
            call.resolve()
        }, failure: { (error) in
            call.reject(error ?? "Unknown error")
        })

        mobileLevel.db_close(dbName, callbacks: callbacks)
    }

    @objc public func batch(_ call: CAPPluginCall) {
        guard let dbName = call.getString("dbName"), let operations = call.getArray("operations", [String: Any].self) else {
            call.reject("Missing dbName or operations")
            return
        }

        let callbacks = MobileLevelCallbacks(success: { _ in
            call.resolve()
        }, failure: { (error) in
            call.reject(error ?? "Unknown error")
        })

        mobileLevel.db_batch(dbName, operations: operations, callbacks: callbacks)
    }

    @objc public func iterator(_ call: CAPPluginCall) {
        guard let dbName = call.getString("dbName"), let options = call.getObject("options") else {
            call.reject("Missing dbName or options")
            return
        }

        let callbacks = MobileLevelCallbacks(success: { result in
            if let r = result as? [String: Any] {
                call.resolve(r)
            } else {
                call.resolve()
            }
        }, failure: { error in
            call.reject(error ?? "Unknown error")
        })

        mobileLevel.db_iterator(dbName, options: options, callbacks: callbacks)
    }

 }
