////
////  MobileLevelTests.m
////  PluginTests
////
////  Created by Aidan Dunlap on 4/29/24.
////  Copyright © 2024 Max Lynch. All rights reserved.
////
//
#import <XCTest/XCTest.h>
#import "Plugin/Plugin-Swift.h"


@interface MobileLevelTests : XCTestCase

@end

@implementation MobileLevelTests

- (void)setUp {
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
}


- (void)testDatabaseOperations {
//    MobileLevel2 *levelDB = [[MobileLevel2 alloc] init];
//    XCTestExpectation *expectation = [self expectationWithDescription:@"Database operations"];
    
    
    
    
}

//
//- (NSString*)generateRandomString:(int)length {
//    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
//    NSMutableString *randomString = [NSMutableString stringWithCapacity:length];
//    for (int i = 0; i < length; i++) {
//        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform((int)[letters length])]];
//    }
//    return randomString;
//}
//
//- (NSString*)generateRandomBase64String:(int)length
//{
//    NSMutableData *randomData = [NSMutableData dataWithLength:length];
//    SecRandomCopyBytes(kSecRandomDefault, length, randomData.mutableBytes);
//    NSString *base64EncodedString = [randomData base64EncodedStringWithOptions:0];
//    return base64EncodedString;
//}
//
//
//- (void)testDatabaseOperations {
//    MobileLevel *levelDB = [[MobileLevel alloc] init];
//    XCTestExpectation *expectation = [self expectationWithDescription:@"Database operations"];
//
//    NSString *dbName = [self generateRandomString:10];
//
//    // Open database
//    CAPPluginCall *openCall = [[CAPPluginCall alloc] init];
//    [openCall setOptions:@{@"dbName": dbName}];
//    [openCall setSuccessHandler:^(id result, CAPPluginCall *call) {
//
//        // Put key-value
//        CAPPluginCall *putCall = [[CAPPluginCall alloc] init];
//        [putCall setOptions:@{@"dbName": dbName, @"key": @"aGVsbG9rZXk=", @"value": @"aGVsbG92YWx1ZQ=="}];
//        [putCall setSuccessHandler:^(id result, CAPPluginCall *call) {
//
//            // Get key-value
//            CAPPluginCall *getCall = [[CAPPluginCall alloc] init];
//            [getCall setOptions:@{@"dbName": dbName, @"key": @"aGVsbG9rZXk="}];
//            [getCall setSuccessHandler:^(CAPPluginCallResult *result, CAPPluginCall *call) {
//                NSDictionary *data = result.resultData;
//                NSString *value = data[@"value"];
//
//                if (![value isEqualToString:@"aGVsbG92YWx1ZQ=="]) {
//                    XCTFail(@"Get operation returned unexpected value: %@", value);
//                }
//
//                // Delete key
//                CAPPluginCall *deleteCall = [[CAPPluginCall alloc] init];
//                [deleteCall setOptions:@{@"dbName": dbName, @"key": @"aGVsbG9rZXk="}];
//                [deleteCall setSuccessHandler:^(id result, CAPPluginCall *call) {
//                    // Check if key was deleted
//                    CAPPluginCall *getCall = [[CAPPluginCall alloc] init];
//                    [getCall setOptions:@{@"dbName": dbName, @"key": @"aGVsbG9rZXk="}];
//                    [getCall setSuccessHandler:^(CAPPluginCallResult *result, CAPPluginCall *call) {
//                        NSDictionary *data = result.resultData;
//                        NSString *value = data[@"aGVsbG92YWx1ZQ=="];
//
//                        if (value != nil) {
//                            XCTFail(@"Get operation should return nil after key deletion but returned: %@", value);
//                        }
//
//                        [expectation fulfill];
//                    }];
//                    [getCall setErrorHandler:^(CAPPluginCallError *error) {
//                        XCTFail(@"Get operation should succeed but failed with error: %@", error.message);
//                        [expectation fulfill];
//                    }];
//                    [levelDB Get:getCall];
//                }];
//                [deleteCall setErrorHandler:^(CAPPluginCallError *error) {
//                    XCTFail(@"Delete operation should succeed but failed with error: %@", error.message);
//                    [expectation fulfill];
//                }];
//                [levelDB Delete:deleteCall];
//            }];
//            [getCall setErrorHandler:^(CAPPluginCallError *error) {
//                XCTFail(@"Get operation should succeed but failed with error: %@", error.message);
//                [expectation fulfill];
//            }];
//            [levelDB Get:getCall];
//        }];
//        [putCall setErrorHandler:^(CAPPluginCallError *error) {
//            XCTFail(@"Put operation should succeed but failed with error: %@", error.message);
//            [expectation fulfill];
//        }];
//        [levelDB Put:putCall];
//    }];
//    [openCall setErrorHandler:^(CAPPluginCallError *error) {
//        XCTFail(@"Database open should succeed but failed with error: %@", error.message);
//        [expectation fulfill];
//    }];
//    [levelDB Open:openCall];
//
//    [self waitForExpectationsWithTimeout:20.0 handler:nil];
//}
//
//- (void)testStressTest {
//    MobileLevel *levelDB = [[MobileLevel alloc] init];
//    XCTestExpectation *expectation = [self expectationWithDescription:@"Stress test"];
//
//    NSString *dbName = [self generateRandomString:10];
//
//    // Open database
//    CAPPluginCall *openCall = [[CAPPluginCall alloc] init];
//    [openCall setOptions:@{@"dbName": dbName}];
//    [openCall setSuccessHandler:^(id result, CAPPluginCall *call) {
//        NSMutableArray *keys = [NSMutableArray array];
//        NSMutableArray *values = [NSMutableArray array];
//        for (int i = 0; i < 1000; i++) {
//            [keys addObject:[self generateRandomBase64String:32]];
//            [values addObject:[self generateRandomBase64String:32]];
//        }
//
//        // Put key-value
//        CAPPluginCall *putCall = [[CAPPluginCall alloc] init];
//        [putCall setOptions:@{@"dbName": dbName, @"key": keys[0], @"value": values[0]}];
//        [putCall setSuccessHandler:^(id result, CAPPluginCall *call) {
//            [expectation fulfill];
//        }];
//        [putCall setErrorHandler:^(CAPPluginCallError *error) {
//            XCTFail(@"Put operation should succeed but failed with error: %@", error.message);
//            [expectation fulfill];
//        }];
//        [levelDB Put:putCall];
//    }];
//    [openCall setErrorHandler:^(CAPPluginCallError *error) {
//        XCTFail(@"Database open should succeed but failed with error: %@", error.message);
//        [expectation fulfill];
//    }];
//    [levelDB Open:openCall];
//
//    [self waitForExpectationsWithTimeout:20.0 handler:nil];
//}
//
//- (void)testIterator {
//    NSString *dbName = [self generateRandomString:10];
//
//    MobileLevel *levelDB = [[MobileLevel alloc] init];
//    XCTestExpectation *expectation = [self expectationWithDescription:@"Iterator"];
//
//    // Open database
//    CAPPluginCall *openCall = [[CAPPluginCall alloc] init];
//    [openCall setOptions:@{@"dbName": dbName}];
//    [openCall setSuccessHandler:^(id result, CAPPluginCall *call) {
//          }];
//
//    [openCall setErrorHandler:^(CAPPluginCallError *error) {
//        XCTFail(@"Database open should succeed but failed with error: %@", error.message);
//        [expectation fulfill];
//    }];
//
//    [levelDB Open:openCall];
//
//    // Clear database
//    CAPPluginCall *clearCall = [[CAPPluginCall alloc] init];
//    [clearCall setOptions:@{@"dbName": dbName, }];
//    [clearCall setSuccessHandler:^(id result, CAPPluginCall *call) {}];
//    [clearCall setErrorHandler:^(CAPPluginCallError *error) {
//        XCTFail(@"Clear operation should succeed but failed with error: %@", error.message);
//        [expectation fulfill];
//    }];
//    [levelDB Clear:clearCall];
//    // generate synthetic data then test gt, gte, lt, lte
//
//    // generate lexicographically sorted keys and values, all 32 bytes all unique
//    NSMutableArray *keys = [NSMutableArray array];
//    NSMutableArray *values = [NSMutableArray array];
//
//    for (int i = 0; i < 1000; i++) {
//        // convert i to 32 bytes
//        NSString *key = [NSString stringWithFormat:@"%032d", i];
//        [keys addObject:key];
//        [values addObject:[self generateRandomBase64String:32]];
//    }
//
//    NSMutableArray *base64Keys = [NSMutableArray array];
//    for (int i = 0; i < 1000; i++) {
//        [base64Keys addObject:[[keys objectAtIndex:i] dataUsingEncoding:NSUTF8StringEncoding]];
//    }
//
//    // Put key-value
//    for (int i = 0; i < 1000; i++) {
//        CAPPluginCall *putCall = [[CAPPluginCall alloc] init];
//        [putCall setOptions:@{@"dbName": dbName, @"key": base64Keys[i], @"value": values[i]}];
//        [putCall setSuccessHandler:^(id result, CAPPluginCall *call) {
//
//        }];
//        [putCall setErrorHandler:^(CAPPluginCallError *error) {
//            XCTFail(@"Put operation should succeed but failed with error: %@", error.message);
//            [expectation fulfill];
//        }];
//        [levelDB Put:putCall];
//    }
//
//    // test no filter
//    {
//        CAPPluginCall *iteratorCall = [[CAPPluginCall alloc] init];
//        [iteratorCall setOptions:@{@"dbName": dbName, }];
//        [iteratorCall setSuccessHandler:^(CAPPluginCallResult *result, CAPPluginCall *call) {
//            NSDictionary *data = result.resultData;
//            NSArray *results = data[@"results"];
//            if ( results.count != 1000) {
//                XCTFail(@"Iterator should return 1000 results but returned %lu", (unsigned long)results.count);
//                [expectation fulfill];
//            }
//        }];
//        [iteratorCall setErrorHandler:^(CAPPluginCallError *error) {
//            XCTFail(@"Iterator should succeed but failed with error: %@", error.message);
//            [expectation fulfill];
//        }];
//
//        [levelDB Iterator:iteratorCall];
//    }
//
//    // test gt
//    {
//        CAPPluginCall *iteratorCall2 = [[CAPPluginCall alloc] init];
//        [iteratorCall2 setOptions:@{@"dbName": dbName, @"gt": base64Keys[500]}];
//        [iteratorCall2 setSuccessHandler:^(CAPPluginCallResult *result, CAPPluginCall *call) {
//            NSDictionary *data = result.resultData;
//            NSArray *results = data[@"results"];
//            if (results.count != 499) {
//                XCTFail(@"Iterator gt should return 499 results but returned %lu", (unsigned long)results.count);
//            }
//
//            NSLog(@"here");
//        }];
//        [iteratorCall2 setErrorHandler:^(CAPPluginCallError *error) {
//            XCTFail(@"Iterator gt should succeed but failed with error: %@", error.message);
//            [expectation fulfill];
//        }];
//
//        [levelDB Iterator:iteratorCall2];
//
//    }
//
//    // test gt and lt
//    {
//        CAPPluginCall *iteratorCall3 = [[CAPPluginCall alloc] init];
//        [iteratorCall3 setOptions:@{@"dbName": dbName, @"gt": base64Keys[500], @"lt": base64Keys[600]}];
//        [iteratorCall3 setSuccessHandler:^(CAPPluginCallResult *result, CAPPluginCall *call) {
//            NSDictionary *data = result.resultData;
//            NSArray *results = data[@"results"];
//            if (results.count != 99) {
//                XCTFail(@"Iterator gt and lt should return 99 results but returned %lu", (unsigned long)results.count);
//            }
//        }];
//        [iteratorCall3 setErrorHandler:^(CAPPluginCallError *error) {
//            XCTFail(@"Iterator gt and lt should succeed but failed with error: %@", error.message);
//            [expectation fulfill];
//        }];
//
//        [levelDB Iterator:iteratorCall3];
//    }
//
//    [expectation fulfill];
//
//    [self waitForExpectationsWithTimeout:20.0 handler:nil];
//
//}

@end
