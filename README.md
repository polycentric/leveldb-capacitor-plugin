# @polycentric/leveldb-capacitor-plugin

LevelDB plugin for Capacitor supporting iOS and Android

## Install

```bash
npm install @polycentric/leveldb-capacitor-plugin
npx cap sync
```
