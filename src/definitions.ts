export interface MobileLevelPluginT {
  open(params: { dbName: string }): Promise<void>;

  get(params: {
    dbName: string;
    key: string;
  }): Promise<{ value: string | null }>;

  put(params: { dbName: string; key: string; value: string }): Promise<void>;

  delete(params: { dbName: string; key: string }): Promise<void>;

  close(params: { dbName: string }): Promise<void>;

  batch(params: {
    dbName: string;
    operations: { type: 'put' | 'delete'; key: string; value?: string }[];
  }): Promise<void>;

  iterator(params: {
    dbName: string;
    options: {
      gt?: string;
      gte?: string;
      lt?: string;
      lte?: string;
      limit?: number;
      reverse?: boolean;
    };
  }): Promise<{ results: [key: string, value: string][] }>;

  clear(params: { dbName: string }): Promise<void>;
}
