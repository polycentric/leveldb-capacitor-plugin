import { encode, decode } from '@borderless/base64';
import { registerPlugin } from '@capacitor/core';
import { AbstractIterator, AbstractLevel } from 'abstract-level';
import type {
  AbstractDatabaseOptions,
  AbstractIteratorOptions,
} from 'abstract-level';

import type { MobileLevelPluginT } from './definitions';

const MobileLevelPlugin = registerPlugin<MobileLevelPluginT>('MobileLevel', {});
export * from './definitions';

type Callback = (err?: Error | null, value?: any) => void;

class DummyIterator extends AbstractIterator<
  MobileLevel,
  Uint8Array,
  Uint8Array
> {
  private readonly options: AbstractIteratorOptions<Uint8Array, Uint8Array>;
  readonly location: string;

  constructor(
    db: MobileLevel,
    location: string,
    options: AbstractIteratorOptions<Uint8Array, Uint8Array>,
  ) {
    super(db, options);
    this.options = options;
    this.location = location;
  }

  // Note: if called by _all() then size can be Infinity. This is an internal
  // detail; by design AbstractIterator.nextv() does not support Infinity.
  _nextv() {
    return;
  }

  _next() {
    return;
  }

  _all(_options: never, callback: Callback) {
    const { gt, gte, lt, lte, ...restOptions } = this.options;

    const encodedOptions = {
      gt: gt ? encode(gt) : undefined,
      gte: gte ? encode(gte) : undefined,
      lt: lt ? encode(lt) : undefined,
      lte: lte ? encode(lte) : undefined,
      ...restOptions,
    };

    MobileLevelPlugin.iterator({
      dbName: this.db.name,
      options: encodedOptions,
    })
      .then(({ results }) => {
        const decodedResults = results.map(
          ([key, value]): [Uint8Array, Uint8Array] => [
            decode(key),
            decode(value),
          ],
        );

        callback(null, decodedResults);
      })
      .catch(err => callback(err, null));
  }

  _seek() {
    return;
  }
}

export class MobileLevel extends AbstractLevel<
  Uint8Array,
  Uint8Array,
  Uint8Array
> {
  public readonly name: string;
  constructor(
    name: string,
    options: AbstractDatabaseOptions<Uint8Array, Uint8Array> | undefined,
  ) {
    super(
      {
        encodings: { view: true },
        // TODO: It technically supports snapshots, I think?
        snapshots: false,
      },
      options,
    );

    this.name = name;
  }

  _open(_options: never, callback: Callback) {
    MobileLevelPlugin.open({
      dbName: this.name,
    })
      .then(() => callback())
      .catch(err => callback(err, null));
  }

  _get(key: Uint8Array, _options: never, callback: Callback) {
    const encodedKey = encode(key);

    MobileLevelPlugin.get({
      dbName: this.name,
      key: encodedKey,
    })
      .then(({ value }) => {
        if (value === null) {
          callback(null, null);
        } else {
          callback(null, decode(value));
        }
      })
      .catch(err => callback(err, null));
  }

  _put(key: Uint8Array, value: Uint8Array, _options: never, callback: Callback) {
    const encodedKey = encode(key);
    const encodedValue = encode(value);

    MobileLevelPlugin.put({
      key: encodedKey,
      value: encodedValue,
      dbName: this.name,
    })
      .then(() => callback())
      .catch(err => callback(err, null));
  }

  _del(key: Uint8Array, _options: never, callback: Callback) {
    const encodedKey = encode(key);

    MobileLevelPlugin.delete({ key: encodedKey, dbName: this.name })
      .then(() => callback())
      .catch(err => callback(err, null));
  }

  _close(callback: Callback) {
    MobileLevelPlugin.close({
      dbName: this.name,
    })
      .then(() => callback())
      .catch(err => callback(err, null));
  }

  _batch(
    operations: {
      type: 'put' | 'delete';
      key: Uint8Array;
      value?: Uint8Array;
    }[],
    _options: never, 
    callback: Callback,
  ) {
    const encodedOperations = operations.map(({ type, key, value }) => ({
      type,
      key: encode(key),
      value: value ? encode(value) : undefined,
    }));

    MobileLevelPlugin.batch({
      operations: encodedOperations,
      dbName: this.name,
    })
      .then(() => callback())
      .catch(err => callback(err, null));
  }

  _iterator(options: {
    gt?: Uint8Array;
    gte?: Uint8Array;
    lt?: Uint8Array;
    lte?: Uint8Array;
    limit?: number;
    reverse?: boolean;
  }): DummyIterator {
    return new DummyIterator(this, this.name, options);
  }

  _clear(_options: never, callback: Callback) {
    MobileLevelPlugin.clear({ dbName: this.name })
      .then(() => callback())
      .catch(err => callback(err, null));
  }
}
